import 'dart:async';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:feedparser/feedparser.dart';

void main() {
  runApp(new MaterialApp(
    home: new LandingPage(),
  ));
}

class LandingPage extends StatefulWidget {
  @override
  createState() => new LandingPageState();
}

class LandingPageState extends State<LandingPage> {
  Feed feed;

  var title;
  var description;
  var image;
  List data;

  Future<String> getData() async {
    http.Response response = await http.get(
        Uri.encodeFull("https://meridian-sport.com/feed/"),
        headers: {"Accept": "application/xml"});

    feed = parse(response.body, strict: true);

    this.setState(() {
      title = feed.title;
      description = feed.description;
      image = feed.image.url;
      data = feed.items.toList();
    });

    print(feed.image.url);

    return "Success!!!";
  }

  @override
  void initState() {
    // TODO: implement initState
    this.getData();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(title),
          backgroundColor: Colors.red,
        ),
        body: new Column(children: <Widget>[
          new Padding(
              padding: EdgeInsets.all(10.0),
              child: new Row(
                children: <Widget>[
                  new Padding(
                      padding: new EdgeInsets.only(left: 30.0),
                      child: Image.network(
                        image,
                        height: 64.0,
                        width: 64.0,
                        alignment: Alignment.center,
                      )),
                  new Padding(
                    padding: new EdgeInsets.all(10.0),
                    child: new Text(
                      description,
                      textAlign: TextAlign.center,
                      style: new TextStyle(
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold,
                          fontStyle: FontStyle.italic),
                    ),
                  ),
                ],
              )),
          new Expanded(
              child: data == null
                  ? const Center(child: const CircularProgressIndicator())
                  : data.length != 0
                      ? new ListView.builder(
                          itemCount: data.length,
                          padding: new EdgeInsets.all(8.0),
                          itemBuilder: (BuildContext context, int index) {
                            return new Card(
                              elevation: 1.7,
                              child: new Padding(
                                  padding: new EdgeInsets.all(10.0),
                                  child: new Column(
                                    children: <Widget>[
                                      new Row(
                                        children: <Widget>[
                                          new Padding(
                                              padding: new EdgeInsets.only(
                                                  left: 4.0),
                                              child: new Icon(Icons.rss_feed)),
                                          new Padding(
                                            padding:
                                                new EdgeInsets.only(left: 5.0),
                                            child: new Text(
                                                feed.items[index].title.length >
                                                        40
                                                    ? feed.items[index].title
                                                            .substring(0, 37) +
                                                        "..."
                                                    : feed.items[index].title),
                                          )
                                        ],
                                      )
                                    ],
                                  )),
                            );
                          })
                      : new Center(
                          child: new Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              new Icon(Icons.chrome_reader_mode,
                                  color: Colors.grey, size: 60.0),
                              new Text(
                                "No articles saved",
                                style: new TextStyle(
                                    fontSize: 24.0, color: Colors.grey),
                              ),
                            ],
                          ),
                        ))
        ]));
  }
}
